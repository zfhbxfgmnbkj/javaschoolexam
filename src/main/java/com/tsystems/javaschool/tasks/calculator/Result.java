package com.tsystems.javaschool.tasks.calculator;

public class Result {
	public boolean success;
    public double result;
    public String[] errors;

    public static Result getSuccess(double result) {
    	Result res = new Result();
        res.success = true;
        res.result = result;
        
        return res;
    }

    public static Result getError(String... errors) {
    	Result res = new Result();
        res.success = false;
        res.errors = errors;
        return res;
    }
}
