package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class Calculator {
	private List<String> parsedExpression;
	private Double lastResult = null;
	private String statement;
	char[] operatorList = { '+', '-', '*', '/', '(', ')'};
	
	public Calculator() {
		parsedExpression = new LinkedList<>();
	}

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
    	String stmt = setStatement(statement);
		if (stmt == null) { return null;}
		Result result = solveExpression();
		if (!result.success) { return null;}
		
		double res = result.result;
		DecimalFormat decimalFormat = new DecimalFormat("#.####", DecimalFormatSymbols.getInstance());
		decimalFormat.setDecimalSeparatorAlwaysShown(false);
		
		return decimalFormat.format(res).replace(",", ".");
    }

    public Result solveExpression() {
		if (parsedExpression.isEmpty()) {
            this.parsedExpression = formatUserInput();
        }
		
		if (parsedExpression.isEmpty())
			return Result.getError("Input cannot be left empty");

		Result res = new Result();
		for (int i = 0; i < parsedExpression.size() - 1; i++) {
			if (parsedExpression.get(i).equals("(")) {
				res = evaluateParentheses(parsedExpression, i);
				if (!res.success)
                    return res;
                i--;
			}
		}
		
		final char[][] orderOfOperations = new char[][] { { '*', '/' }, { '+', '-' } };
		for (char[] operators : orderOfOperations) {
			for (int i = 1; i < parsedExpression.size(); i++) {
				char inputOp = parsedExpression.get(i).charAt(0);
				for (char op : operators) {
					if (op == inputOp) {
						res = condenseExpression(op, i);
						if (!res.success)
                            return res;
						
						if (op == '/' && Double.isInfinite(res.result))
							return Result.getError("Cannot divide by zero");
						
						parsedExpression.remove(i + 1); 
                        parsedExpression.remove(i); 
                        parsedExpression.set(i - 1, Double.toString(res.result));
                        i--;
					}
				}
			}
		}
		
		if (parsedExpression.size() == 1) {
			double statement;
			try {
				statement = Double.parseDouble(parsedExpression.get(0));
            } catch (Exception e) {
                return Result.getError("Not a number", "Error parsing input");
            }
			lastResult = statement;
			
            return Result.getSuccess(statement);
		} else {
            return Result.getError("Could not resolve expression");
        }
	}
	
	public Result condenseExpression(char operator, int i) {
		if (i - 1 < 0 || i + 1 >= parsedExpression.size()) {
            return Result.getError("Operator requires two numbers");
        }
		
		String left = parsedExpression.get(i - 1);
		Result x = catchNumberException(left);
        Result y = catchNumberException(parsedExpression.get(i + 1));
        
        if (!x.success)
            return x;
        if (!y.success)
            return y;
        
        switch (operator) {
        case '/':
            return Result.getSuccess(x.result / y.result);
        case '*':
            return Result.getSuccess(x.result * y.result);
        case '-':
            return Result.getSuccess(x.result - y.result);
        case '+':
            return Result.getSuccess(x.result + y.result);
        }
        
        return Result.getError("Invalid operator");
	}
	
	private Result catchNumberException(String elem) {
        double result;
        try {
            result = Double.parseDouble(elem); // value of i-1
        } catch (NumberFormatException e) {
            return Result.getError("Operator requires two numbers");
        }
        return Result.getSuccess(result);
    }
	
	Result evaluateParentheses(List<String> formattedList, int start) {
		Map<Integer, Integer> relatedParentheses = evaluateRelations(formattedList);

		if (relatedParentheses.isEmpty())
            return Result.getError("Parentheses mismatch");
		
		int end = relatedParentheses.get(start);
		
		if (start + 1 == end)
            return Result.getError("Input cannot be left blank");
		
		Calculator newCalculator = new Calculator();
		newCalculator.statementList(new LinkedList<String>(formattedList.subList(start + 1, end)));
		Result res = newCalculator.solveExpression();
		
		for (int t = end; t > start; t--)
            formattedList.remove(t);
		
		if (res.success)
            formattedList.set(start, Double.toString(res.result));
		
        return res;
	}
	
	HashMap<Integer, Integer> evaluateRelations(List<String> formattedList) {
		Stack<Integer> openingParenthesis = new Stack<>();
		HashMap<Integer, Integer> relationships = new HashMap<>();
		
		for (int i = 0; i < formattedList.size(); i++) {
			char c = formattedList.get(i).charAt(0);
			if (c == '(') {
				openingParenthesis.push(i);
			} else if (c == ')' && openingParenthesis.size() > 0) {
                relationships.put(openingParenthesis.pop(), i);
            }
		}
		
		if (openingParenthesis.empty())
            return relationships;
		
		return new HashMap<Integer, Integer>();
	}
	
	public List<String> formatUserInput() {
		statement = statement.replaceAll("\\s", "");
		
		List<String> formattedList = new LinkedList<>();
		int start = 0;
		for (int i = 0; i < statement.length(); i++) {
			for (char operator : operatorList) {
				if (operator == statement.charAt(i)) {
					String prefix = statement.substring(start, i);
					if (prefix.length() > 0)
						formattedList.add(prefix);
					formattedList.add(Character.toString(operator));
                    start = i + 1;
                    break;
				}
			}
		}
		
		if (statement.equals(""))
            return formattedList;
		
		String remainder = statement.substring(start);
        if (!remainder.equals(""))
            formattedList.add(remainder);
        
		return formattedList;
	}
	
	String setStatement(String statement) {
		if (statement == null) {
			return null;
		}
		this.statement = statement;
		return statement;
	}

	void statementList(List<String> subList) {
		this.parsedExpression = subList;
	}
}
