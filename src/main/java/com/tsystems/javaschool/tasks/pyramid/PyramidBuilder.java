package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {
	static int elsInRow;

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
    	if (inputNumbers.size() > 1000) {
    		throw new CannotBuildPyramidException(
    				"Input data is too big");
    	}
    	
    	int[] source = inputNumbers.stream()
    			                   .peek(p -> { if (p == null) throw new CannotBuildPyramidException("Cannot build pyramid from null values"); })
    			                   .sorted()
    			                   .mapToInt(i -> i).toArray();
    	int len = source.length;
    	
    	if (len == 0) {
    		throw new CannotBuildPyramidException(
    				"Cannot build pyramid from empty input");
    	}
    	
    	int height = getNumRows(len);
    	
    	if (height == -1) {
    		throw new CannotBuildPyramidException(
    				"Cannot build pyramid from input of length " + len);
    	}
    	
    	int width = 2 * height - 1;
		int[][] pyramid = new int[height][width];
		
		int index = 0;
		
		int middle = width / 2;
		pyramid[0][middle] = source[index];
		
		for (int i = 0; i < height; i++) {
			if (i > 0) {
				int[] insert = getSubarray(source, i, index);
				System.arraycopy(insert, 0, pyramid[i], middle - i, insert.length);
			}
			elsInRow = i + 1;
			index += elsInRow;
		}
    	
        return pyramid;
    }

    int[] getSubarray(int[] array, int row, int index) {
		int len = row + row + 1;
		int[] result = new int[len];

		if (index < array.length) {
			int buff_size = row + 1;
			int[] buff = new int[buff_size];
			System.arraycopy(array, index, buff, 0, buff_size);
			
			int buf_idx = 0;
			for (int i = 0; i < len; i++) {
				if (i % 2 == 0)
					result[i] = buff[buf_idx++];
				else
					result[i] = 0;
			}
		}

		return result;
	}

    int getNumRows(int n) {
		int idx = 0;
		int numRows = 0;
		int numInRow = 0;
		
		while (idx < n) {
			numRows++;
			for (numInRow = 0; numInRow < numRows; numInRow++) {
				idx++;
			}
		}
		
		return idx == n ? numRows : -1;
	}
}
